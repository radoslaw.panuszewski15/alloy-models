//=========================================================
// Zad. 1
// 1. Uruchom Alloy Analyzer.
// 2. Wczytaj niniejszy plik.
// 3. Wybierz 'Execute' aby uruchomic polecenie 'run'.
// 4. Wybierz 'Show' aby wyswietlic instancje modelu.
// 5. Wybierz 'Next' aby wyswietlic kolejna instancje.
// 6. Stopniowo rozszerzaj model i analizuj dopisujac
// kolejne ograniczenia. Analizujac model przegladaj
// zawsze kilka jego instancji.
//=========================================================

sig Gang { members: set Inmate }

sig Inmate { room: Cell }

sig Cell { }

pred show {
  // W miescie sa dwa gangi
  //#Gang = 2
  
  // Kazdy gang ma czlonkow (w wiezieniu) 
  all g: Gang | some g.members
 
  // Kazdy osadzony jest czlonkiem ktoregos z gangow
  all i: Inmate | one g: Gang | i in g.members

  // Nie ma pustych cel
  all c: Cell | some i: Inmate | i.room = c

  // W kazdej celi moze siedziec nie wiecej niz dwoch
  // osadzonych
  all c: Cell | #c.~room <= 2

  // Czlonkowie tego samego gangu nie siedza w tej
  // samej celi
  all c: Cell | #c.~room > 1 => #c.~room.~members > 1
}

run show
